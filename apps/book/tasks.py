import os

from datetime import timedelta

from django.utils import timezone
from django.core.mail import send_mail

from celery import shared_task

from .models import Order

@shared_task
def order_email_notification():
    orders = (Order.objects.filter(is_notified=False, created_at__lte=timezone.now() - timedelta(hours=1, minutes=30))
              .values('id', 'user__email'))
    Order.objects.filter(id__in=[order['id'] for order in orders]).update(is_notified=True)
    for order in orders:
        send_mail(
            'Congrats!',
            'Congrats to buy book',
            os.environ.get('EMAIL_HOST_USER'),
            [order['user__email']]
        )
