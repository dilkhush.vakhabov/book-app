from rest_framework import serializers
from .models import Author, Book, OrderItems, Order
from django.db.models import F


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class BookAuthorsSerializer(serializers.ModelSerializer):
    authors = AuthorSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = '__all__'


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('id', 'title', 'price', 'released_date',)


class CartSerializer(serializers.ModelSerializer):
    book = BookSerializer(read_only=True)

    class Meta:
        model = OrderItems
        fields = ('id', 'book', 'quantity',)

    def validate(self, attrs):
        book_id = self.context['request'].data.get('book_id', None)
        if book_id is None:
            raise serializers.ValidationError({'book_id': 'field is required'})
        return attrs

    def create(self, validated_data):
        book_id = self.context['request'].data.get('book_id')
        try:
            orderitem = OrderItems.objects.get(book__pk=book_id)
            orderitem.quantity = F('quantity') + validated_data['quantity']
            orderitem.save(update_fields=('quantity',))
            orderitem.refresh_from_db()
            return orderitem
        except OrderItems.DoesNotExist:
            try:
                book = Book.objects.get(pk=book_id)
                return OrderItems.objects.create(user=self.context['request'].user, book=book, **validated_data)
            except Book.DoesNotExist:
                raise serializers.ValidationError({'message': 'book not found'})


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ('id', 'created_at',)

    def create(self, validated_data):
        orderitems = OrderItems.objects.filter(user=self.context['user'], order__isnull=True)
        if orderitems.count() == 0:
            raise serializers.ValidationError({'detail': 'your cart is empty please add one book to cart'})
        order = Order.objects.create(user=self.context['user'])
        orderitems.update(order=order)
        return order
