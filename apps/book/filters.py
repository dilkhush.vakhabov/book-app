import django_filters
from .models import Book


class BookFilter(django_filters.FilterSet):
    min_price = django_filters.NumberFilter(field_name="price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(field_name="price", lookup_expr='lte')
    min_date = django_filters.DateFilter(field_name="released_date", lookup_expr='gte')
    max_date = django_filters.DateFilter(field_name="released_date", lookup_expr='lte')

    class Meta:
        model = Book
        fields = ['title', 'authors', 'price', 'released_date', 'min_price', 'max_price', 'min_date', 'max_date']
