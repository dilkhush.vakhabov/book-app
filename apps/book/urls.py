from django.urls import path
from rest_framework import routers
from .views import BookViewSet, CartViewSet, OrderListCreateView

router = routers.SimpleRouter()
router.register('book', BookViewSet)
router.register('cart', CartViewSet)

urlpatterns = [
    path('order/', OrderListCreateView.as_view())
] + router.urls
