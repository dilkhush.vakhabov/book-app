from rest_framework import viewsets, permissions, generics
from django_filters import rest_framework as filters
from .models import Book, OrderItems, Order
from .serializers import BookAuthorsSerializer, CartSerializer, OrderSerializer
from .permissions import BookPermission
from .filters import BookFilter


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    filterset_class = BookFilter
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = (BookPermission,)
    serializer_class = BookAuthorsSerializer


class CartViewSet(viewsets.ModelViewSet):
    queryset = OrderItems.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = CartSerializer

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user, order__isnull=True)

    def get_serializer_context(self):
        return {'request': self.request}


class OrderListCreateView(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_serializer_context(self):
        return {'user': self.request.user}
