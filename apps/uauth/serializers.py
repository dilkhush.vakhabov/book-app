from django.contrib.auth import authenticate
from rest_framework import serializers, validators
from apps.uauth.models import User

class UserRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128, write_only=True)
    password2 = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=64, read_only=True)

    def create(self, validated_data):
        password = validated_data.get('password')
        password2 = validated_data.pop('password2')

        if User.objects.filter(email=validated_data['email']).count() > 0:
            raise validators.ValidationError({'message': 'this email already exists'})

        if password != password2:
            raise validators.ValidationError({'message': 'password not matched'})

        return User.objects.create_user(**validated_data)


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=64, read_only=True)

    def validate(self, attrs):
        user = authenticate(**attrs)

        if user is None:
            raise serializers.ValidationError({'message': 'email or password incorrect'})
        return user
