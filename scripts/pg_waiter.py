import socket
import time
import os

port = int(os.environ.get("POSTGRES_PORT", 5432))

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect(('db', port))
        s.close()
        time.sleep(5)
        break
    except socket.error as ex:
        time.sleep(0.1)
